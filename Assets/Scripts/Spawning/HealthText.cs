﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthText : MonoBehaviour {

    public int score;
    //float transformX = -3.21f;
    float transformX = -0.165f;
    //float transformY = -3.21f;
    float transformY = -0.165f;
    float transformZ = 0.0f;

    public void Start() {
        //Place the health text on the ally
        placeHealth();
        /*Vector3 position = Util.ally.transform.position;
        position.x += transformX;
        position.y += transformY;
        position.z += transformZ;
        transform.position = position;*/
        //transform.position = Util.ally.transform.position;
        //transform.position.x += transformX;

        // Rotate the score display to face the player
        //transform.rotation = Camera.main.transform.rotation;
    }

    void Update() {
        //Keep the health text on the ally
        placeHealth();
        //transform.position = Util.ally.transform.position;

        // Ensure score text keeps facing player
        //transform.rotation = Camera.main.transform.rotation;
    }

    void placeHealth()
    {
        // Ensure score text keeps facing player
        //transform.rotation = Camera.main.transform.rotation;

        //Place the health text on the ally
        Vector3 position = Util.ally.transform.position;
        /*position.x += transformX;
        position.y += transformY;
        position.z += transformZ;*/
        transform.position = position;

        //Move up slightly to centre on the cow itself rather than the ground underneath
        transform.rotation = Util.ally.transform.rotation;
        //transform.Translate(0.0f, 1.6f, 0.0f);
        transform.Translate(0.0f, 0.016f, 0.0f);

        // Ensure health text keeps facing player
        transform.rotation = Camera.main.transform.rotation;

        //Move the text
        transform.Translate(transformX, transformY, transformZ);
    }
}
