﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Creator : MonoBehaviour {
    // Wall and Spawners
    public static MeshRenderer[] wallMeshes = null;
    public static float totalWallLength;
    public static MeshPointHandler MeshHandler;
    public static Transform levelTransform;
    List<GameObject> spawners = null;

    // Object Pool
    ObjectPooler objectPooler;

    [Header("Prefabs")]
    public GameObject enemyPrefab;
    public GameObject spawnerPrefab;

    // [Header("Tags - Should match with pool tags")]
    string spiderTag = "Spider";
    string grassTag = "Grass";
    string spawnerTag = "Spawner";

    [Header("Toggle Settings")]
    public bool createGrassPatchesOnStart;
    public bool createGrassOverTime;
    public bool createSpawnersOverTime;

    #region Grass Creation
    [Header("Grass")]
    [Range(0, 1)] public float onKillSpawnChance = 0.1f;
    GameObject grassParent;
    Coroutine grassOverTimeCoroutine;

    [Header("Grass spawn on start")]
    public int numGrassPatches = 1;
    public IntRange grassPatchSize = new IntRange(10, 50);
    public FloatRange grassPatchRadius = new FloatRange(0.1f, 0.25f);

    [Header("Grass spawn over time")]
    public FloatRange grassTickTime = new FloatRange(1, 10);
    public IntRange grassTickCount = new IntRange(1, 5);
    #endregion

    #region Spawner Creation
    [Header("Spawners created over time")]
    public FloatRange spawnerCreationTickTime = new FloatRange(5, 20);
    public IntRange spawnerTickCount = new IntRange(1, 4);
    Coroutine spawnersOverTimeCoroutine;

    [Header("Spiders created from spawners")]
    public IntRange numSpidersPerSpawner = new IntRange(20, 50);
    public FloatRange spiderCreationTickTime = new FloatRange(10.0f, 30.0f);
    public IntRange spiderTickCount = new IntRange(1, 5);
    #endregion 

    // NOTE: Remove at end submission
    [Header("Debugging [REMOVE AT END]")]
    public KeyCode createSpawner = KeyCode.W;
    public KeyCode createMultipleSpawners = KeyCode.S;
    public KeyCode createEnemy = KeyCode.Space;
    public KeyCode createMultipleEnemies = KeyCode.Return;
    public int spawnersCreatedPerMultiKeypress = 5;
    public int enemiesCreatedPerMultiKeypress = 50;

    #region Singleton Usage
    public static Creator Instance;

    private void Awake() {
        Instance = this;
    }
    #endregion

    void Update() {
        ReadKeyInput();
    }

    public void Initialize() {
        // SetupPools for grass and spiders
        SetupPools();

        // Setup wall meshes
        wallMeshes = gameObject.GetComponentsInChildren<MeshRenderer>();

        //Find the total length of wall
        totalWallLength = 0.0f;
        for (int i = 0; i < wallMeshes.Length; i++)
        {
            totalWallLength += wallMeshes[i].bounds.size.y / 2.0f;
        }

        Init();
    }

    public void Initialize(Mesh mesh, Transform level)
    {
        // SetupPools for grass and spiders
        SetupPools();

        MeshHandler = new MeshPointHandler(mesh);
        levelTransform = level;

        Init();
    }

    private void Init()
    {
        spawners = new List<GameObject>();

        // Create grass parent to store all grasses
        grassParent = new GameObject();
        grassParent.name = "Grasses";
        grassParent.transform.parent = transform;

        // Generate starting grass patch if enabled
        if (createGrassPatchesOnStart)
            CreateGrassPatch();

        // Start grass generation coroutine if enabled
        if (createGrassOverTime)
            grassOverTimeCoroutine = StartCoroutine(GenerateGrassOverTime());

        // Start spawner generation over time coroutine if enabled
        if (createSpawnersOverTime)
            spawnersOverTimeCoroutine = StartCoroutine(GenerateSpawnersOverTime());
    }

    void SetupPools() {
        // Setup pooler
        objectPooler = ObjectPooler.Instance;
        objectPooler.SetupPools();
    }

    void ReadKeyInput() {
        // Only enable keys when spawners are not made over time
        if (!createSpawnersOverTime) {
            // DEBUG: Spawn one spawner
            if (Input.GetKeyDown(createSpawner))
            {
                CreateSpawnerAtRandomLocation();
            }

            // DEBUG: Spawn multiple spawners
            else if (Input.GetKeyDown(createMultipleSpawners))
            {
                for (int i = 0; i < spawnersCreatedPerMultiKeypress; ++i)
                {
                    CreateSpawnerAtRandomLocation();
                }
            }

            // DEBUG: Spawn one bug
            else if (Input.GetKeyDown(createEnemy))
            {
                SpawnEnemyKeyPress();
            }

            // DEBUG: Spawn multiple bugs
            else if (Input.GetKeyDown(createMultipleEnemies))
            {
                for (int i = 0; i < enemiesCreatedPerMultiKeypress; ++i)
                    SpawnEnemyKeyPress();
            }
        }
    }

    // ########## Destroying creations ##########
    public void DestroyCreations() {
        // Set chance to spawn grass to 0
        onKillSpawnChance = 0.0f;

        // Halt coroutines
        if (createGrassOverTime) {
            StopCoroutine(grassOverTimeCoroutine);
        }
        if (createSpawnersOverTime) {
            StopCoroutine(spawnersOverTimeCoroutine);
        }

        // Kill all alive spiders/spawners
        foreach(Transform child in levelTransform) {
            Spawner spawner = child.GetComponent<Spawner>();

            if (spawner) {
                spawner.EndGame();
            } else {
                Enemy enemy = child.GetComponent<Enemy>();
                enemy.EndGame();
            }
        }
    }

    // ########## Grass Spawning ##########
    #region
    public bool IsGrassPoolEmpty() {
        return objectPooler.IsPoolEmpty(grassTag);
    }

    // Spawn grass when spider killed
    public GameObject SpawnGrassAtPosition(Vector3 spawnPoint, Quaternion spawnRotation) {
        if (objectPooler.IsPoolEmpty(grassTag)) {
            // Debug.Log("[ERROR] - Cannot spawn any more grass");
            return null;
        }

        // Spawning via retrieving from pool
        return objectPooler.SpawnFromPool(grassTag, grassParent.transform, spawnPoint, spawnRotation);
    }

    public GameObject SpawnGrassAtPosition(Vector3 spawnPoint)
    {
        MeshPointHandler.Point point = MeshHandler.ClosestPoint(spawnPoint);
        if (objectPooler.IsPoolEmpty(grassTag))
        {
            // Debug.Log("[ERROR] - Cannot spawn any more grass");
            return null;
        }

        // Spawning via retrieving from pool
        return objectPooler.SpawnFromPool(grassTag, grassParent.transform, point.position, Quaternion.LookRotation(point.normal));
    }

    GameObject SpawnGrassAtRandomPosition() {
        return SpawnAtRandomLocation(levelTransform, grassTag);
    }

    IEnumerator GenerateGrassOverTime() {
        while (true) {
            // Set randomized time and amount spawned on this tick call
            float delayTime = Random.Range(grassTickTime.min, grassTickTime.max);
            int numSpawnedAtCall = Random.Range(grassTickCount.min, grassTickCount.max);
            GameObject spawnedGrass = null;

            // Spawn grass
            for (int i = 0; i < numSpawnedAtCall; i++)
                spawnedGrass = SpawnGrassAtRandomPosition();

            // Wait till next time for spawning grass if can still spawn grass
            yield return new WaitForSeconds(delayTime);

            // Stop self if out of grass to spawn
            if (spawnedGrass == null) {
                // Debug.Log("Stopped grass spawn coroutine");
                StopCoroutine(grassOverTimeCoroutine);
                yield return null;
            }
        }
    }

    void CreateGrassPatch() {
        // Setup variables
        int patchSize;
        float patchRadius;
        Vector3 spawnPoint;

        // Create gameobject in order to obtain up and right vectors, this is where grass patches will be placed
        GameObject spawnArea = new GameObject();

        // Repeat for number of grass patches
        for (int i = 0; i < numGrassPatches; i++) {
            // Generate random value from min and max for patch size and patch radius
            patchSize = Random.Range(grassPatchSize.min, grassPatchSize.max);
            patchRadius = Random.Range(grassPatchRadius.min, grassPatchRadius.max);

            // Selecting random point to create grass patch in
            MeshPointHandler.Point point = MeshHandler.RandomPoint();
            Quaternion rotation = Quaternion.LookRotation(point.normal);

            spawnPoint = point.position;
            spawnArea.transform.position = spawnPoint;
            spawnArea.transform.rotation = rotation;

            // Populate grass in radius of grass patch
            for (int grassInPatch = 0; grassInPatch < patchSize; grassInPatch++) {
                // Reference: https://stackoverflow.com/questions/34675771/unity-c-sharp-spawning-gameobjects-randomly-around-a-point

                // Add randomized offset for both up and right vectors
                spawnPoint += spawnArea.transform.up * Random.Range(-patchRadius, patchRadius);
                spawnPoint += spawnArea.transform.right * Random.Range(-patchRadius, patchRadius);
                GameObject grassSpawned = SpawnGrassAtPosition(spawnPoint, rotation);

                // Stop when out of grass
                if (grassSpawned == null) {
                    // Debug.Log("Cannot generate any more patches... Stopping grass patch generation");
                    return;
                }
            }
        }

        // Remove spawn area at end
        Destroy(spawnArea);
    }
    #endregion

    // ########## Enemy Spawning ##########
    #region
    void SpawnEnemyKeyPress() {
        if (spawners.Count == 0) {
            Debug.Log("[ERROR] - Spawner is required to spawn spiders");
            return;
        }

        if (objectPooler.IsPoolEmpty(spiderTag)) {
            Debug.Log("[ERROR] - Pool is empty, spider not spawned");
            return;
        }

        // Select a spawner to create spider from
        GameObject spawnerSelected = Util.SelectRandomFromList(spawners);
        Vector3 spawnPoint = spawnerSelected.transform.position;
        Quaternion spawnRotation = spawnerSelected.transform.rotation;
        
        // Grab spider from pool and initialize it
        objectPooler.SpawnFromPool(spiderTag, spawnerSelected.transform, spawnPoint, spawnRotation);
    }

    public void SpawnEnemy(GameObject spawnerParent) {
        if (objectPooler.IsPoolEmpty(spiderTag)) {
            // Debug.Log("[ERROR] - Cannot spawn any more grass");
            return;
        }

        // Grab spider from pool and create it
        objectPooler.SpawnFromPool(spiderTag, spawnerParent.transform, spawnerParent.transform.position, spawnerParent.transform.rotation);
    }
    #endregion
    
    // ########## Spawner Creation ##########
    #region
    void CreateSpawnerAtRandomLocation() {
        SpawnAtRandomLocation(levelTransform, spawnerTag);
    }

    IEnumerator GenerateSpawnersOverTime() {
        while (true) {
            // Set randomized time
            float delayTime = Random.Range(spawnerCreationTickTime.min, spawnerCreationTickTime.max);
            
            // Do when there is a spawner to create, otherwise do nothing
            if (!objectPooler.IsPoolEmpty(spawnerTag)) {
                // Set randomized amount spawned on this tick call
                int numSpawnedAtCall = Random.Range(spawnerTickCount.min, spawnerTickCount.max);

                // Create spawners
                for (int i = 0; i < numSpawnedAtCall; i++)
                {
                    SpawnAtRandomLocation(levelTransform, spawnerTag);
                }

                // Wait till next time for spawning spawners
                yield return new WaitForSeconds(delayTime);
            }
            else
                yield return new WaitForSeconds(delayTime);
        }
    }
    #endregion

    // ########## Generic Functions ########## 
    GameObject SpawnAtRandomLocation(Transform parent, string poolTag) {
        MeshPointHandler.Point point = MeshHandler.RandomPoint();

        GameObject spawnedObject = objectPooler.SpawnFromPool(poolTag, parent, point.position, Quaternion.LookRotation(point.normal));
        return spawnedObject;
    }
}
