﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupText : MonoBehaviour {
    // References:
    // https://www.youtube.com/watch?v=_ICCSDmLCX4
    // https://www.youtube.com/watch?v=fbUOG7f3jq8

    Animator animator;

    void Start() {
        // Get animation
        animator = GetComponent<Animator>();
        AnimatorClipInfo[] animationInfo = animator.GetCurrentAnimatorClipInfo(0);
        float animationDuration = animationInfo[0].clip.length;

        // Ensure popup text is facing player
        transform.rotation = Camera.main.transform.rotation;

        // Destroy pop up text after animation finished
        Destroy(gameObject, animationDuration);
    }

    void Update() {
        // Ensure popup text is facing player
        transform.rotation = Camera.main.transform.rotation;
    }
}
