﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

    // Tutorial Link: https://www.youtube.com/watch?v=tdSmKaJvCoA
    [Header("* Endless: Object returns back to pool when done")]
    public List<Pool> pools;
    public Dictionary<string, Pool> poolDictionary;

    #region Singleton Usage
    public static ObjectPooler Instance;

    private void Awake() {
        Instance = this;
    }
    #endregion

    public void SetupPools() {
        poolDictionary = new Dictionary<string, Pool>();

        // Create general pool parent to contain all pools
        GameObject objectPools = new GameObject();
        objectPools.name = "ObjectPools";
        Transform poolParent = objectPools.transform;
        poolParent.parent = transform;

        // Go through each pool and set it up
        foreach (Pool pool in pools) {
            GeneratePool(pool, poolParent);
            poolDictionary.Add(pool.tag, pool);
        }
    }

    void GeneratePool(Pool poolParams, Transform parent) {
        // Debug.Log("Generated pool");

        // Create Pool
        Transform pool = new GameObject().transform;
        pool.parent = parent;
        pool.name = poolParams.tag + "Pool";
        Debug.Log(poolParams.tag);

        // Populate pool based on its size
        for (int i = 0; i < poolParams.size; i++) {
            GameObject obj = Instantiate(poolParams.prefab);
            IPooledObject pooledObj = obj.GetComponent<IPooledObject>();
            pooledObj.OnObjectInstantiated();

            poolParams.SetPoolTransform(pool);
            poolParams.EnqueuePooledObject(obj);
        }
    }

    public GameObject SpawnFromPool(string tag, Transform parent, Vector3 position, Quaternion rotation) {
        if (!poolDictionary.ContainsKey(tag)) {
            Debug.LogWarning("Cannot find " + tag + " in pool");
            return null;
        }

        // Get pool to use
        Pool pool = poolDictionary[tag];

        // Check if pool is empty
        if (pool.IsEmpty()) {
            Debug.LogWarning("Pool containing: " + tag + " is empty");
            return null;
        }

        // Retrieve object from pool
        GameObject objectSpawned = pool.DequeuePooledObject();
        objectSpawned.transform.position = position;
        objectSpawned.transform.rotation = rotation;
        objectSpawned.transform.parent = parent;

        // Get pooledObject component and call its on spawn function
        IPooledObject pooledObj = objectSpawned.GetComponent<IPooledObject>();
        if (pooledObj != null) {
            pooledObj.OnObjectSpawnedFromPool(pool);
        }

        return objectSpawned;
    }

    public bool IsPoolEmpty(string tag) {
        return poolDictionary[tag].IsEmpty();
    }

    public int GetPoolCount(string tag) {
        return poolDictionary[tag].GetPoolCount();
    }
}
