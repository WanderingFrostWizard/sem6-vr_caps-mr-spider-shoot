﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Pool 
{
    public string tag;
    public GameObject prefab;
    public int size;
    public bool endless;

    #region Pooled Objects
    public Queue<GameObject> pooledObjects = new Queue<GameObject>();
    Transform poolTransform;
    bool empty;

    public void EnqueuePooledObject(GameObject objectToEnqueue) {
        objectToEnqueue.SetActive(false);
        objectToEnqueue.transform.parent = poolTransform;
        pooledObjects.Enqueue(objectToEnqueue);
    }

    public GameObject DequeuePooledObject() {
        GameObject objectRetrieved = pooledObjects.Dequeue();
        objectRetrieved.SetActive(true);
        return objectRetrieved;
    }

    public void SetPoolTransform(Transform transform) {
        poolTransform = transform;
    }

    public bool IsEmpty() {
        if (pooledObjects.Count == 0)
            return true;

        return false;
    }

    public int GetPoolCount() {
        return pooledObjects.Count;
    }

    public void ReducePoolSize() {
        size--;
    }
    #endregion
}