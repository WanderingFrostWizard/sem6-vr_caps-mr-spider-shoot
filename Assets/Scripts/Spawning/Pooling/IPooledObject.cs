﻿using UnityEngine;

public interface IPooledObject {
    void OnObjectSpawnedFromPool(Pool pool);
    void OnObjectFinished();
    void OnObjectInstantiated();
}
