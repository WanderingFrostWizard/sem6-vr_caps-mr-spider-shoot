﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour, IPooledObject {
    // Spawner pool
    Pool spawnerPool;

    // Spider counts
    [SerializeField] int numSpidersToSpawn;
    [SerializeField] int numSpidersLeft;

    // Spider generation
    Coroutine spiderGenerationCoroutine;
    FloatRange spiderCreationTickTime;
    IntRange spiderTickCount;

    // References
    Creator creator;

    void Initialize() {
        // Obtain reference to creator class and setup values
        creator = Creator.Instance;
        spiderCreationTickTime = creator.spiderCreationTickTime;
        spiderTickCount = creator.spiderTickCount;
        numSpidersToSpawn = Random.Range(creator.numSpidersPerSpawner.min, creator.numSpidersPerSpawner.max);
        numSpidersLeft = numSpidersToSpawn;
    }

    // Kill all spiders inside spawner, call when game has ended
    public void EndGame() {
        // Stop spider spawning coroutine
        StopCoroutine(spiderGenerationCoroutine);
        StartCoroutine(EndGameBehaviour());
    }

    IEnumerator EndGameBehaviour() {
        while (transform.childCount > 1) {
            // Skip first child (model for spawner)
            for (int i = 1; i < transform.childCount; i++) {
                Enemy enemy = transform.GetChild(i).GetComponent<Enemy>();
                enemy.EndGame();
            }

            yield return new WaitForSeconds(1.0f);
        }

        // Destroy self when done
        Destroy(gameObject);
    }

    IEnumerator GenerateSpidersOverTime() {
        while (numSpidersLeft > 0) {
            // Set randomized time and amount of spiders to spawn
            float delayTime = Random.Range(spiderCreationTickTime.min, spiderCreationTickTime.max);
            int numSpawnedAtCall = Random.Range(spiderTickCount.min, spiderTickCount.max);

            yield return new WaitForSeconds(delayTime);

            // Spawn spiders
            for (int i = 0; i < numSpawnedAtCall; i++) {
                creator.SpawnEnemy(gameObject);
                numSpidersLeft--;

                // Exit if out of spiders to spawn
                if (numSpidersLeft == 0)
                    break;
            }
        }

        OnObjectFinished();
    }

    #region Pooled Object Interface Implementation
    public void OnObjectSpawnedFromPool(Pool pool) {
        // Assign reference to pool
        spawnerPool = pool;

        // Start coroutine to spawn spiders
        spiderGenerationCoroutine = StartCoroutine(GenerateSpidersOverTime());
        // spiderGenerationCoroutine = StartCoroutine(GenerateSpidersOverTime());
    }

    public void OnObjectFinished() {
        // Detach any spiders (if spawner runs out of spiders to spawn and is destroyed)
        if (transform.childCount > 1) 
        {
            for (int i = 1; i < transform.childCount; i++) 
            {
                transform.GetChild(i).parent = transform.parent;
            }
        }

        if (spawnerPool.endless) {
            // Reset num spiders to generate to starting value
            numSpidersLeft = numSpidersToSpawn;
            spawnerPool.EnqueuePooledObject(gameObject);
        }
        else {
            spawnerPool.ReducePoolSize();
            Destroy(gameObject);
        }
    }

    public void OnObjectInstantiated() {
        Initialize();
    }
    #endregion
}
