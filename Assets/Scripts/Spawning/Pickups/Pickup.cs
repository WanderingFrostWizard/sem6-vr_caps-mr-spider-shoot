﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {
    //void Start() {
    //    Initialize();
    //}

    //void Initialize() {
    
    //}

    // When box collider is entered
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Ally") {
            Ally ally = other.transform.root.GetComponent<Ally>();
            ObtainPickup(ally);
        }
    }

    // To be overridden in child
    public virtual void ObtainPickup(Ally ally) {

    }
}
