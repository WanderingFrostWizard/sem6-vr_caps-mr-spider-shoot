﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassPickup : Pickup, IPooledObject {
    // Takes 1 second to eat a grass blade
    Animator animator;
    Pool grassPool;
    List<GameObject> grassBlades;

    float timeTakenToEatGrassBlade = 1.0f;

    // References
    GameManager gameManager; // to game manager (Enable update health + score)

    public override void ObtainPickup(Ally ally) {
        if (!ally.IsEating()) {
            ally.StartEating(transform);
            StartCoroutine(EatGrass(ally));
        }
    }

    IEnumerator EatGrass(Ally ally) {
        // Total time taken == number of grass blades
        int timer = transform.GetChild(0).childCount;

        // Make a copy for grassblades to consume
        List<GameObject> grassBladesToEat = new List<GameObject>(grassBlades);

        while (timer >= 1) {
            yield return new WaitForSeconds(timeTakenToEatGrassBlade);

            timer--;
            // Select random grassblade to eat, remove it from the list when eaten
            GameObject grassEaten = Util.SelectRandomFromList(grassBladesToEat);
            grassEaten.SetActive(false);

            // Remove it from possible grassblades to eat
            grassBladesToEat.Remove(grassEaten);
        }

        // Once done, finish eating and destroy self
        ally.EndEating();
        OnObjectFinished();

        // Update score and display popup text
        gameManager.AteGrass(transform.position);
    }

    #region Pooled Object Interface Implementation
    public void OnObjectSpawnedFromPool(Pool pool) {
        // Assign pool as reference
        grassPool = pool;

        // Create array of grassblades to eat
        grassBlades = Util.GetChildren(transform.GetChild(0).gameObject);

        // Get animation and play it
        animator = GetComponent<Animator>();
        animator.enabled = true;
        animator.Play("GrassAnimation");

        // Functionality on spawned here
    }

    public void OnObjectFinished() {
        if (grassPool.endless) {
            // Toggle grassblades to be visible again and return them to the pool
            foreach (GameObject grassBlade in grassBlades)
                grassBlade.SetActive(true);

            grassPool.EnqueuePooledObject(gameObject);
        }
        else {
            grassPool.ReducePoolSize();
            Destroy(gameObject);
        }
    }

    public void OnObjectInstantiated() {
        gameManager = GameManager.Instance;
    }
    #endregion
}
