﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnStartScript : MonoBehaviour
{

  // BASIC SCRIPT TO SIMPLY CALL THE NEXT SCENE, THIS IS USED
  // FOR THE SOLE PURPOSE OF AVOIDING ANY DUPLICATION OF THE 
  // VR ASSETS
  public SceneTransition sceneTrans;

	// Use this for initialization
	void Start ()
  {
    sceneTrans.MoveToScene();

  }
}
