﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util : MonoBehaviour
{
    // Global values
    public static Vector3 allyTarget;

    // This is used as the enemyTarget
    public static Vector3 allyLastPosition;
    public static GameObject ally;
    public static bool dead;

    //---=== NAVIGATION / NAVMESH Variables ===---
    // List of NAVIGATION constants
    public static float AGENTSIZE = 0.5f;

    // Extra padding added by unity to the edges of each NavMeshSurface
    public static float PADDING = 0.4f;

    // ########################################################
    // ALL WALLS and positions are scaled based upon this VALUE
    public static float wallScaleValue = 5;
    // ########################################################

    // Each plane is 10 units wide at a scale of 1,1,1
    public static float PLANEORIGSIZE = 10;

    // I use the following variable to reference half of the current planes size
    public static float planeSize;
    // Find the level object where ALL objects will be attached
    public static GameObject levelLayout;

    // This boolean is set AFTER the level loading has completed in LevelSetup.cs [NOT NEEDED CURRENTLY]
    // public static bool levelsLoaded = false;

    // https://forum.unity.com/threads/hiow-to-get-children-gameobjects-array.142617/
    public static List<GameObject> GetChildren(GameObject go) {
        List<GameObject> children = new List<GameObject>();
        foreach (Transform tran in go.transform) {
            children.Add(tran.gameObject);
        }
        return children;
    }

    public static T SelectRandom<T>(params T[] options)
    {
        return options[Random.Range(0, options.Length)];
    }

    public static T SelectRandomFromList<T>(List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }

    public static T WrapIndex<T>(IList<T> array, int i)
    {
        i %= array.Count;
        if(i < 0)
        {
            i += array.Count;
        }
        return array[i];
    }

    public static Vector3 VectorForSideRegularPolygon(int side, int numSides)
    {
        float angle = (float)side / numSides; // [0,1)
        angle *= 360; // [0,360)
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.up);
        return rotation * Vector3.forward;
    }

    public static void LockCursor() {
        // Hide cursor
        UnityEngine.Cursor.visible = false;
        UnityEngine.Cursor.lockState = CursorLockMode.Locked;
    }
}
