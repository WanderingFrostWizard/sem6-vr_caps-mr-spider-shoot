﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class Triangulator {
    private class Vertex
    {
        public int index;
        public Vector2 point;
        public Vertex prev;
        public Vertex next;
        public bool isReflex;
        public bool isConvex;

        public Vertex(Vector2 point, int index)
        {
            this.point = point;
            this.index = index;
        }

        /**
         * https://math.stackexchange.com/questions/1324179/how-to-tell-if-3-connected-points-are-connected-clockwise-or-counter-clockwise
         * https://en.wikipedia.org/wiki/Curve_orientation
         **/
        public bool IsClockwise()
        {
            float determinant = prev.point.x * point.y + next.point.x * prev.point.y + point.x * next.point.y - prev.point.x * next.point.y - next.point.x * point.y - point.x * prev.point.y;
            return determinant <= 0;
        }

        public void CalculateIfReflexOrConvex()
        {
            isReflex = false;
            isConvex = false;

            if(IsClockwise())
            {
                isReflex = true;
            }
            else
            {
                isConvex = true;
            }
        }

        /**
         * From http://totologic.blogspot.se/2014/01/accurate-point-in-triangle-test.html
         **/
        public bool PointInside(Vector2 point)
        {
            //Based on Barycentric coordinates
            float denominator = ((this.point.y - next.point.y) * (prev.point.x - next.point.x) + (next.point.x - this.point.x) * (prev.point.y - next.point.y));

            float a = ((this.point.y - next.point.y) * (point.x - next.point.x) + (next.point.x - this.point.x) * (point.y - next.point.y)) / denominator;
            float b = ((next.point.y - prev.point.y) * (point.x - next.point.x) + (prev.point.x - next.point.x) * (point.y - next.point.y)) / denominator;
            float c = 1 - a - b;

            //The point is within the triangle
            return (a > 0f && a < 1f && b > 0f && b < 1f && c > 0f && c < 1f);
        }

        public bool IsEar(List<Vertex> vertices)
        {
            // A reflex vertex can't be an ear
            if (isReflex) return false;

            bool anyInside = vertices.Any(vertex =>
                // only need to check reflex vertices
                vertex.isReflex
                && PointInside(vertex.point)
            );

            return !anyInside;
        }

        public int[] Triangle()
        {
            return new[]
            {
                prev.index, index, next.index
            };
        }

        public void RemoveFrom(List<Vertex> v)
        {
            v.Remove(this);
            next.prev = prev;
            prev.next = next;
        }
    }

    private static void AddIfEar(Vertex v, List<Vertex> vertices, List<Vertex> earVertices)
    {
        if(v.IsEar(vertices))
        {
            earVertices.Add(v);
        }
    }

    public static int[] Triangulate(Vector2[] polygon)
    {
        if (polygon.Length < 3)
        {
            Debug.LogError("Polygon has less than 3 vertices");
            return null;
        }
        else if (polygon.Length == 3)
        {
            return new int[] { 0, 1, 2 };
        }

        var vertices = polygon.Select((vertex, i) => new Vertex(vertex, i)).ToList();

        // build links
        for(var i = 0; i < vertices.Count; ++i)
        {
            var vertex = vertices[i];
            vertex.prev = Util.WrapIndex(vertices, i - 1);
            vertex.next = Util.WrapIndex(vertices, i + 1);
            vertex.CalculateIfReflexOrConvex();
        }
        
        var earVertices = vertices.Where(vertex => vertex.IsEar(vertices)).ToList();
        var indices = new List<int>();

        while(vertices.Count > 3)
        {
            // Pop next ear
            Vertex ear = earVertices[0];
            earVertices.RemoveAt(0);

            var prev = ear.prev;
            var next = ear.next;
            indices.AddRange(ear.Triangle());
            ear.RemoveFrom(vertices);

            // Check for new ear by checking adjacent vertices
            prev.CalculateIfReflexOrConvex();
            next.CalculateIfReflexOrConvex();

            earVertices.Remove(prev);
            earVertices.Remove(next);

            AddIfEar(prev, vertices, earVertices);
            AddIfEar(next, vertices, earVertices);
        }

        indices.AddRange(vertices.Select(vertex => vertex.index));
        return indices.ToArray();
    }

    public static int[] TriangulateConvex(Vector2[] polygon)
    {
        var indices = new int[polygon.Length * 3];
        for (var i = 0; i < polygon.Length - 2; ++i)
        {
            var idx = i * 3;
            indices[idx + 0] = 0;
            indices[idx + 1] = i + 1;
            indices[idx + 2] = i + 2;
        }
        return indices;
    }
}
