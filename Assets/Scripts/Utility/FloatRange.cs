﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FloatRange {
    public float min;
    public float max;

    public FloatRange(float minimum, float maximum) {
        min = minimum;
        max = maximum;
    }
}
