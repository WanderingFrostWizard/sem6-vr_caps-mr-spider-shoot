﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class IntRange {
    public int min;
    public int max;

    public IntRange(int minimum, int maximum) {
        min = minimum;
        max = maximum;
    }
}
