﻿using System.Linq;
using UnityEngine;

public class MeshPointHandler
{
    public struct Point
    {
        public Vector3 position;
        public Vector3 normal;
    }

    private readonly Mesh mesh;
    private float[] sizes;
    private float[] cumulativeSizes;

    public MeshPointHandler(Mesh mesh)
    {
        this.mesh = mesh;
        int triCount = this.mesh.triangles.Length / 3;
        sizes = new float[triCount];
        cumulativeSizes = new float[triCount];
        TriangleSizes();
    }

    private void TriangleSizes()
    {
        for (int i = 0; i < sizes.Length; i++)
        {
            int index = i * 3;
            Vector3 a = mesh.vertices[mesh.triangles[index + 0]];
            Vector3 b = mesh.vertices[mesh.triangles[index + 1]];
            Vector3 c = mesh.vertices[mesh.triangles[index + 2]];
            // Magnitude of cross product of two vectors gives area of parallelegram defined by the vectors
            Vector3 cross = Vector3.Cross(a - b, a - c);
            // Area of triangle is half area of parallelegram
            sizes[i] = cross.magnitude / 2;
            cumulativeSizes[i] = (i == 0 ? 0 : cumulativeSizes[i - 1]) + sizes[i];
        }
    }

    public Point RandomPoint()
    {
        int triangle = RandomTriangle();
        Debug.Assert(triangle != -1, "Triangle not found");
        return new Point
        {
            position = RandomBarycentric(triangle),
            normal = mesh.normals[mesh.triangles[triangle * 3]]
        };
    }

    private int RandomTriangle()
    {
        float total = cumulativeSizes[sizes.Length - 1];
        float randomTriangle = Random.value * total;
        for (int i = 0; i < sizes.Length; i++)
        {
            if (randomTriangle <= cumulativeSizes[i])
            {
                return i;
            }
        }
        return -1;
    }

    private Vector3 RandomBarycentric(int triangle)
    {
        int index = triangle * 3;
        Vector3 a = mesh.vertices[mesh.triangles[index + 0]];
        Vector3 b = mesh.vertices[mesh.triangles[index + 1]];
        Vector3 c = mesh.vertices[mesh.triangles[index + 2]];

        float r = Random.value;
        float s = Random.value;

        if(r + s >= 1)
        {
            r = 1 - r;
            s = 1 - s;
        }

        return a + r * (b - a) + s * (c - a);
    }

    /*
     * From https://math.stackexchange.com/questions/544946/determine-if-projection-of-3d-point-onto-plane-is-within-a-triangle
     */
    private Vector3? ProjectionInTriangle(Vector3 p, Vector3 a, Vector3 b, Vector3 c)
    {
        Vector3 u = b - a;
        Vector3 v = c - a;
        Vector3 n = Vector3.Cross(u, v);
        float nsqr = n.sqrMagnitude;
        Vector3 w = p - a;
        float gamma = Vector3.Dot(Vector3.Cross(u, w), n) / nsqr;
        if (gamma < 0 || gamma > 1)
        {
            return null;
        }

        float beta = Vector3.Dot(Vector3.Cross(w, v), n) / nsqr;
        if (beta < 0 || beta > 1)
        {
            return null;
        }

        float alpha = 1 - gamma - beta;
        if (alpha < 0 || alpha > 1)
        {
            return null;
        }
        return alpha * a + beta * b + gamma * c;
    }

    public Point ClosestPoint(Vector3 p)
    {
        var closest = new Point();
        float closestDistance = Mathf.Infinity;

        for(int i = 0; i < mesh.triangles.Length; i += 3)
        {
            Vector3 a = mesh.vertices[mesh.triangles[i + 0]];
            Vector3 b = mesh.vertices[mesh.triangles[i + 1]];
            Vector3 c = mesh.vertices[mesh.triangles[i + 2]];
            Vector3? projection = ProjectionInTriangle(p, a, b, c);
            if(projection.HasValue)
            {
                float distance = (projection.Value - p).sqrMagnitude;
                if(distance < closestDistance)
                {
                    closestDistance = distance;
                    closest.position = projection.Value;
                    closest.normal = mesh.normals[mesh.triangles[i]];
                }
            }
        }

        return closest;
    }
}
 