﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour {

    [Header("Scene to load via MoveToScene()")]
    public string sceneToMoveTo;

    Scene currentScene;
    bool sceneLoading;

    void Start() {
        Initialize();
    }

    void Initialize() {
        currentScene = SceneManager.GetActiveScene();
    }

    // Button usable functions
    public void ReloadCurrentScene() {
        LoadScene(currentScene.name);
    }

    public void MoveToScene() {
        LoadScene(sceneToMoveTo);
    }

    public void MoveToTitle() {
    LoadScene("Title_Screen");
    }

    public void ReplayGame()
    {
      LoadScene("Game");
    }

    public void EndGame()
    {
      LoadScene("End_Game_Screen");
    }

    public void QuitGame() {
      #if UNITY_EDITOR
          UnityEditor.EditorApplication.isPlaying = false;
      #else
          Application.Quit();
      #endif
      }


    // Internal function
    void LoadScene(string scene) {
        if (!sceneLoading) {
            sceneLoading = true;
            SceneManager.LoadScene(scene);
        }
    }
}
