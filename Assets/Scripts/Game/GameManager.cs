﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    [Header("Level Parameters")]
    public bool beginGameOnStart = false;
    public float timerDuration = 10.0f;
    public float cowStartingHealth = 100.0f;
    public string endgameScreen = "Title_Screen";

    // Tracked values
    GameObject popupParent;
    bool gameEnded;
    bool gamePaused;
    int score;
    float cowHealth;
    int killCount = 0;

    [Header("References")]
    public HUDManager hudManager;
    public GameObject allyPrefab;
    public GameObject popupText;
    public GameObject popupTextHealth;
    public GameObject scoreText;
    public GameObject healthText;
    public Text spidersKilledText;
    public Text endScoreText;
    AStarLevelSetup aStarLevelSetup;
    Creator creator;
    GameObject ally;

    [Header("Settings")]
    public int grassHealthRestore = 3;
    public int pointsPerGrassEaten = 1;
    public int spiderDamage = -1;

    string pointsPerGrassEatenString = "+";
    string spiderDamageString;

    [Header("Debugging Keys")]
    public KeyCode startTimer = KeyCode.C;
    public KeyCode pauseEditor = KeyCode.P;
    public KeyCode quitApp = KeyCode.Escape;

    public enum GameResult { WIN, LOSE };
    float timeTillSceneTransition = 5.0f;

    #region Singleton Usage
    public static GameManager Instance;

    private void Awake() {
        Instance = this;
    }
    #endregion

    void Start() {
        Initialize();
    }

    void Initialize() {
        cowHealth = cowStartingHealth;
        hudManager.Initialize(timerDuration, this);
        gamePaused = true;
        gameEnded = false;

        creator = GetComponent<Creator>();
        aStarLevelSetup = GetComponent<AStarLevelSetup>();

        // Setup strings
        pointsPerGrassEatenString += pointsPerGrassEaten.ToString();
        spiderDamageString = spiderDamage.ToString();

        // Create text popup parent to contain text popups
        popupParent = new GameObject();
        popupParent.transform.parent = transform;
        popupParent.name = "PopupTexts";

        scoreText.SetActive(true);
        healthText.SetActive(true);

        Util.LockCursor();

        // Spawn ally at feet
        ally = Instantiate(allyPrefab);
        ally.transform.position = new Vector3(0, 0, 0);
        Util.ally = ally;

        if (beginGameOnStart) {
            TogglePaused();
        }
    }

    void Update() {
        if (gameEnded == false && cowHealth <= 0.0f)
        {
            EndGame(GameResult.LOSE);
        }

        // REMOVE AT END submission
        if (Input.GetKeyDown(startTimer)) {
            TogglePaused();
        }

        else if (Input.GetKeyDown(pauseEditor)) {
            EditorApplication.isPaused = true;
        }

        else if (Input.GetKeyDown(quitApp)) {
            UnityEditor.EditorApplication.isPlaying = false;
        }

        if (Input.GetKeyDown(KeyCode.X)) {
            Util.dead = true;
            hudManager.HideDisplays();

            Ally allyComponent = ally.GetComponent<Ally>();
            allyComponent.Kill();
            EndGame(GameResult.LOSE);
        }
    }

    #region Game State
    public void TogglePaused() {
        gamePaused = !gamePaused;
        hudManager.PauseTimer(gamePaused);
    }

    public void EndGame(GameResult result)
    {
        gameEnded = true;
        End_Game_Manager.EndGameData.spiderKills = killCount;
        End_Game_Manager.EndGameData.grassEaten = GetScore();
        End_Game_Manager.EndGameData.result = result;

        if (result == GameResult.WIN)
            creator.DestroyCreations();

        StartCoroutine(LoadEndGameScreen());
    }

    IEnumerator LoadEndGameScreen() {
        yield return new WaitForSeconds(timeTillSceneTransition);
        SceneManager.LoadScene("End_Game_Screen");
    }

    #endregion

    #region Enemy/Grass called functions
    public void AteGrass(Vector3 location) {
        // Grass eaten
        AddToScore(pointsPerGrassEaten);
        ChangeHealth(grassHealthRestore);
        SpawnPopupText(location, pointsPerGrassEatenString);
    }

    public void AllyAttacked(Vector3 location) {
        // Spider attacked ally
        ChangeHealth(spiderDamage);
        SpawnHealthText(location, spiderDamageString);
    }
    #endregion

    #region Health
    public void InitHealthDisplay()
    {
        hudManager.ShowHealthDisplay(transform.position);
    }

    public string GetHealthText()
    {
        return cowHealth.ToString();
    }

    public float GetHealth()
    {
        return cowHealth;
    }

    void ChangeHealth(int change)
    {
        cowHealth += change;

        // Prevent going over max health
        if (cowHealth > cowStartingHealth) {
            cowHealth = cowStartingHealth;
        }

        // 0 HP, cow died, player loses
        if (cowHealth <= 0.0f) {
            Util.dead = true;
            hudManager.HideDisplays();

            Ally allyComponent = ally.GetComponent<Ally>();
            allyComponent.Kill();
            EndGame(GameResult.LOSE);
        }
        else {
            hudManager.UpdateHealthDisplay(cowHealth);
        }
    }

    void SpawnHealthText(Vector3 position, string value)
    {
        GameObject popup = Instantiate(popupTextHealth, position, Quaternion.identity, popupParent.transform);
        popup.transform.GetChild(0).GetComponent<TextMesh>().text = value;
    }

    #endregion

    #region Tracked variables
    public GameObject GetAlly() {
        return ally;
    }

    public string GetKillCount() {
        return killCount.ToString();
    }

    public void IncrementKillCount() {
        killCount++;
    }
    #endregion

    #region Scoring
    public void InitScoreDisplay() {
        hudManager.ShowScoreDisplay(transform.position);
    }

    public string GetScoreText() {
        return score.ToString();
    }

    int GetScore() {
        return score;
    }

    public void SpawnPopupText(Vector3 position, string value) {

        GameObject popup = Instantiate(popupText, position, Quaternion.identity, popupParent.transform);
        popup.transform.GetChild(0).GetComponent<TextMesh>().text = value;
    }

    public void AddToScore(int value) {
        score += value;
        hudManager.UpdateScoreDisplay(score);
    }
    #endregion
}
