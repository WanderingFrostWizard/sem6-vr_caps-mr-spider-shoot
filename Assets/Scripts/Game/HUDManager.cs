﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {
    // References
    GameManager gameManager;
    Timer levelTimer;
    GameObject hudParentScore;
    GameObject hudParentHealth;

    // Result window
    public Transform gameResultWindow;
    Transform loseWindow;
    Transform winWindow;
    Text killCount;
    Text finalScore;

    //public GameObject scoreText;
    GameObject hudScoreText;
    TextMesh scoreText;
    GameObject hudHealthText;
    TextMesh healthText;


    public void Initialize(float timerDuration, GameManager manager) {
        // Setup references
        gameManager = manager;
        levelTimer = transform.Find("LevelTimer").GetComponent<Timer>();

        // Game over window
        loseWindow = gameResultWindow.Find("LoseWindow");
        winWindow = gameResultWindow.Find("WinWindow");
        killCount = gameResultWindow.Find("Results/KillCount/Value").GetComponent<Text>();
        finalScore = gameResultWindow.Find("Results/FinalScore/Value").GetComponent<Text>();

        // Create a HUD parent to contain score display
        hudParentScore = new GameObject();
        hudParentScore.transform.parent = transform;
        hudParentScore.name = "HUDScoreText";

        // Create a HUD parent to contain health display
        hudParentHealth = new GameObject();
        hudParentHealth.transform.parent = transform;
        hudParentHealth.name = "HUDHealthText";

        // Hide game over display at the start
        gameResultWindow.gameObject.SetActive(false);

        // Startup timer
        levelTimer.Initialize(timerDuration);
    }

    public void PauseTimer(bool state) {
        levelTimer.PauseTimer(state);
    }

    public void ShowScoreDisplay(Vector3 position) {
        hudScoreText = Instantiate(gameManager.scoreText, position, Quaternion.identity, hudParentScore.transform);
        scoreText = hudScoreText.transform.GetChild(0).GetComponent<TextMesh>();
        scoreText.text = gameManager.GetScoreText();
    }

    public void UpdateScoreDisplay(float newScore) {
        scoreText.text = newScore.ToString();
    }

    public void ShowHealthDisplay(Vector3 position) {
        hudHealthText = Instantiate(gameManager.healthText, position, Quaternion.identity, hudParentHealth.transform);
        healthText = hudHealthText.transform.GetChild(0).GetComponent<TextMesh>();
        healthText.text = gameManager.GetHealthText();
    }

    public void HideDisplays() {
        hudHealthText.SetActive(false);
        hudScoreText.SetActive(false);
    }

    public void UpdateHealthDisplay(float newHealth) {
        healthText.text = newHealth.ToString();
    }

    void SetResults() {
        killCount.text = gameManager.GetKillCount();
        finalScore.text = gameManager.GetScoreText();
    }

    public void DisplayGameOverScreen(GameManager.GameResult result) {
        SetResults();

        gameResultWindow.gameObject.SetActive(true);

        // Determine whether player won or lost
        if (result == GameManager.GameResult.WIN) {
            winWindow.gameObject.SetActive(true);
            loseWindow.gameObject.SetActive(false);
        }
        else {
            winWindow.gameObject.SetActive(false);
            loseWindow.gameObject.SetActive(true);
        }
    }
}
