﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CowHUDManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Debug.Log("CowHUD initialised");
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Place the HUD on the ally
        transform.position = Util.ally.transform.position;

        //Move up slightly to centre on the cow itself rather than the ground underneath
        transform.rotation = Util.ally.transform.rotation;
        transform.Translate(0.0f, 0.0321f, 0.0f);

        //transform.RotateAround(Util.ally.transform.position, new Vector3(1, 0, 0), 90.0f);
        transform.Rotate(90.0f, 0.0f, 0.0f);

        // Ensure the HUD keeps facing player
        //transform.rotation = Camera.main.transform.rotation;

        //Debug.Log("Canvas position: x = " + transform.position.x + ", y = " + transform.position.y + ", z = " + transform.position.z);
        //Debug.Log("Canvas rotation: x = " + transform.rotation.x + ", y = " + transform.rotation.y + ", z = " + transform.rotation.z);
        //Debug.Log("Ally rotation: x = " + Util.ally.transform.rotation.x + ", y = " + Util.ally.transform.rotation.y + ", z = " + Util.ally.transform.rotation.z);
    }
}
