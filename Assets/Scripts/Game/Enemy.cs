﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(IAstarAI))]
public class Enemy : MonoBehaviour, IPooledObject
{
    IAstarAI ai;
    // static float detectRangeFactor = 0.25f; // Detection range of spider AI is a fraction of the total wall length (in Creator.cs)
    static float detectRange = 2.5f;
    static float attackRange = 0.02f;        // How close do the spiders need to get before they can attack? Picked 4.0f based on testing for what felt right, might need to change if we change level scaling
    Vector3 moveTo;
    bool destinationSet = false;

    Creator creator;
    float grassSpawnChance;

    public Animator animator;
    bool attacking = false;
    string attackingBoolTag = "Attacking";
    float spiderAttackTimer = 1.3f;         // Spiders attack depending on animation speed
    float spiderAttackDelay = 1.0f;         // Slight delay before next attack

    bool start = false;
    // References
    GameManager gameManager; // to game manager (Enable update health + score)

    void Initialize() {
        creator = Creator.Instance;
        gameManager = GameManager.Instance;
        grassSpawnChance = creator.onKillSpawnChance;
    }
    
    void Update()
    {
      SpiderBehaviour();
    }
    
    public void MoveToLocation(Vector3 targetPoint)
    {
        ai.destination = targetPoint;
        ai.canMove = true;
    }

    void SpiderBehaviour() {
        if (start) {
            // Target ally only when its alive
            if (!Util.dead && (Vector3.Distance(transform.position, Util.ally.transform.position) <= detectRange)) {
                moveTo = Util.ally.transform.position;
                destinationSet = false;
            }
            else if (destinationSet == false) {
                Debug.Log("Random search");
                MoveToRandomPoint();
                destinationSet = true;
            }
            else if (Vector3.Distance(transform.position, moveTo) <= detectRange / 5.0f) {
                MoveToRandomPoint();
            }

            MoveToLocation(moveTo);

            if (Vector3.Distance(transform.position, Util.ally.transform.position) <= attackRange)
            {
                StartAttacking();
            }
        }
    }
    
    void MoveToRandomWall()
    {
        //Pick a random number within the total wall length
        float spawnLoc = Random.Range(0, Creator.totalWallLength);
        float spawnLocDistLeft = spawnLoc;

        //Find which wall that corresponds to
        for (int i = 0; i < Creator.wallMeshes.Length; i++)
        {
          //If the spawn location lies on this wall, spawn on this wall
          if (spawnLocDistLeft <= Creator.wallMeshes[i].bounds.size.y / 2.0f)
          {
            Transform current = Creator.wallMeshes[i].transform.parent;
            Vector3 wallSize = Creator.wallMeshes[i].bounds.size;
            Vector3 wallMin = -(wallSize / 2.0f) + current.transform.position;
            Vector3 wallMax = (wallSize / 2.0f) + current.transform.position;
            MoveInBounds(current, wallMin, wallMax);
            break;
          }
          else //Otherwise, move on to the next wall
          {
            spawnLocDistLeft -= Creator.wallMeshes[i].bounds.size.y / 2.0f;
          }
        }
    }
    private void MoveToRandomPoint()
    {
        moveTo = Creator.MeshHandler.RandomPoint().position;
    }
    
    void MoveInBounds(Transform parent, Vector3 min, Vector3 max)
  {
    // Generate random x, y, z values in min and max ranges for spawn location
    float xPos = Random.Range(min.x, max.x);
    float yPos = Random.Range(min.y, max.y);
    float zPos = Random.Range(min.z, max.z);
    moveTo = new Vector3(xPos, yPos, zPos);
  }
    
    // Natural kill
    public void Kill() {
        gameManager.IncrementKillCount();
        StartCoroutine(PlayAnimationThenDie());
    }

    // Called when game has ended, don't update score
    public void EndGame() {
        StartCoroutine(PlayAnimationThenDie());
    }
    
    IEnumerator PlayAnimationThenDie() {
        // Stop movement
        start = false;
        ai.canMove = false;

        // Play death animation
        animator.Play("SpiderDieAnimation");
        float animationLength = 2.5f;
        yield return new WaitForSeconds(animationLength);

        OnObjectFinished();
    }

    #region Attacking
    IEnumerator Attack()
    {
        yield return new WaitForSeconds(spiderAttackTimer);

        // Once complete, set the attack flag to false
        EndAttacking();

        yield return new WaitForSeconds(spiderAttackDelay);
    }

    // ##### Attack toggling #####
    public void StartAttacking()
    {
        if (!attacking) {
            ToggleAttacking(true);
            StartCoroutine(Attack());
        }
    }

    public void EndAttacking()
    {
        gameManager.AllyAttacked(transform.position);
        ToggleAttacking(false);
    }

    void ToggleAttacking(bool state)
    {
        attacking = state;
        animator.SetBool(attackingBoolTag, state);
    }
    #endregion

    #region Pooling Functionality
    Pool spiderPool;
    public void OnObjectFinished() {
        // Check if there are grass to spawn
        if (!creator.IsGrassPoolEmpty()) {
            // Spawn grass with chance
            if (Random.value <= grassSpawnChance) {
                // Set parent to wall, pass in current spider's transform to set positioning and rotation of grass
                creator.SpawnGrassAtPosition(transform.position);
            }
        }

        // Check if pool is endless
        if (spiderPool.endless)
            spiderPool.EnqueuePooledObject(gameObject);
        else {
            spiderPool.ReducePoolSize();
            Destroy(gameObject);
        }
    }

    public void OnObjectSpawnedFromPool(Pool pool) {
        spiderPool = pool;

        // Setup nav mesh agent
        ai = GetComponent<IAstarAI>();
        start = true;
    }
    
    public void OnObjectInstantiated() {
        Initialize();
    }
    #endregion
}