﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircleHealthBar : MonoBehaviour {
     public Image _bar;
    float healthValue = 100.0f;
     //References
     GameManager gameManager;
     
     public void Start()
     {
         gameManager = GameManager.Instance;
     }

     //Update is called once per frame
    void Update ()
    {
    //Place the health bar on the ally
    //transform.position = Util.ally.transform.position;

    //Move up slightly to centre on the cow itself rather than the ground underneath
    //transform.rotation = Util.ally.transform.rotation;
    //transform.Translate(0.0f, 1.6f, 0.0f);

     //Ensure health bar keeps facing player
    //transform.rotation = Camera.main.transform.rotation;

    //Update for current health value
     HealthChange(gameManager.GetHealth());
    }

    public void HealthChange(float newHealth)
    {
        float amount = newHealth / 100.0f;
        _bar.fillAmount = amount;
    }
}
