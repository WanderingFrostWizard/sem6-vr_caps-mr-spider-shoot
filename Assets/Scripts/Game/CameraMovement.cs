﻿using UnityEngine;
using System.Collections;
using UnityEngine.XR.WSA.Input;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using HoloToolkit.Unity.InputModule;

public class CameraMovement : MonoBehaviour
{
  public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 };

  [Header("Movement Axis")]
  public RotationAxes axes = RotationAxes.MouseXAndY;
  public float sensitivityX = 15F;
  public float sensitivityY = 15F;
  public FloatRange xRange = new FloatRange(-360f, 360f);
  public FloatRange yRange = new FloatRange(-90f, 90f);
  float rotationX = 0F;
  float rotationY = 0F;
  Quaternion originalRotation;

  // Reference to the projectileManger GameObject
  // This is to be the PARENT of ALL projectiles
  [Header("Projectiles")]
  public GameObject projectileManager;
  // Projectile prefab
  public GameObject projectilePrefab;

  // Currently selected gameObject to allow easy access and editing
  private GameObject currObj;
  [Header("Input")]
  // Swaps the shoot / move functionality to better suit left or right handed players
  public bool rightHanded = true;
  public bool pcMouseInput = false;
  // VR Controller variables
  // NOTE these VARIABLES MUST be set within the update function
  // as the controller objects are not instantiated until a few
  // updates into the game... 
  private GameObject rightController;
  public GameObject rightPointer;
  private GameObject leftController;
  public GameObject leftPointer;
  // Boolean value used to indicate that the controller variables above have been set
  private bool setupComplete = false;

  // Point impacted by the raycast
  private RaycastHit hit;
  // If we actually hit something
  private bool hitSomething = false;
  private Vector3 targetDir;

  // Position Markers
  [Header("Position Marker")]
  public GameObject allyLastPosMarkerPrefab;
  public float allyMarkerOffset = 2.0f;
  private GameObject allyLastPosMarker;

  [Header("Debugging Keys")]
  public KeyCode snapToScreen = KeyCode.O;

  void Start()
  {
    // Create target markers
    addLastPosMarker();

    originalRotation = transform.localRotation;

    // Set our Projectile Manager to dont Destroy, as this is needed in the main game
    DontDestroyOnLoad(projectileManager);     
  }

  private void addLastPosMarker()
  {
    allyLastPosMarker = Instantiate(allyLastPosMarkerPrefab, new Vector3(0, -Util.wallScaleValue * 100, 0), Quaternion.identity);
    //allyLastPosMarker.transform.localScale = allyMarkerSize;
  }

  void Update()
  {
    if (Input.GetKeyDown(snapToScreen)) {
        Util.LockCursor();
    }

    // ######################### CONTROLLER SETUP #############################
    if ( pcMouseInput )
    {
      // Ignore the controller setup below as it is NOT needed
    }
    else
    {
      // Check if controllers have been setup
      if (!setupComplete)
      {
        if (SetupControllers())
        {
          setupComplete = true;
          Debug.Log("Setup Complete");
        }
        // Stop the rest of the update function from running UNTIL the controllers have been
        // detected to stop errors
        else
        {
          return;
        }
      }
    }
    
    // ####################### CAMERA MOVEMENT ################################
    if (pcMouseInput)
    {
      updateCameraView();
      // Set the direction here for any shoot or move commands
      targetDir = Camera.main.transform.forward;
    }
    else
    {
      // DO NOTHING AS VR Controllers move on their own using gaze controls
    }

    if ( SceneManager.GetActiveScene().name == "Title_Screen" )
    {
      // Debug.Log("In Title Screen ");

      // IGNORE other shoot methods
      return;
    }




    // ################# MOUSE / CONTROLLER INTERACTIONS ######################
    if (Input.GetMouseButtonDown(0) && Input.GetMouseButtonDown(1))
    {
      // DO NOTHING
    }
    // Left Mouse Button / RIGHT Controller
    else if (Input.GetButtonDown("Shoot"))
    {
      //Debug.Log("SHOOT");

      // If using VR Controllers
      if (!pcMouseInput)
      {
        targetDir = rightPointer.transform.forward;
      }

      // Target WITH EITHER THE VR Controllers or Mouse
      if (rightHanded)
      {
        Shoot(targetDir);
      }
      else
      {
        Move();
      }
    }
    // Right Mouse Button / Left Controller
    else if (Input.GetButtonDown("Move"))
    {
      //Debug.Log("MOVE");

      // If using VR Controllers
      if (!pcMouseInput)
      {
        targetDir = leftPointer.transform.forward;
      }

      if ( rightHanded )
      {
        Move(); 
      }
      else
      {
        Shoot(targetDir);
      }
    }
  }

  // Move the allyPosMarker to the RayCastHit location
  private void Move( )
  {
     // Debug.Log("Moving");

     Physics.Raycast(gameObject.transform.position, targetDir, out hit, 2000);
     
     Util.allyLastPosition = hit.point;
     
     // Check if marker has been destroyed
     if ( allyLastPosMarker == null )
       addLastPosMarker();
     
     // Place marker at hit position, rotate it to sit on top of the hit point
     Transform hitObject = hit.transform;
     allyLastPosMarker.transform.rotation = Quaternion.LookRotation(hit.normal); //   rotation;
     // Match position and add tiny offset to position
     //Vector3 offset = allyLastPosMarker.transform.up * allyMarkerOffset;
     allyLastPosMarker.transform.position = hit.point;
  }

  private void Shoot( Vector3 target )
  {
    // Debug.Log("SHOOTING");

    // Create bullet, destroy it after 2 seconds passed
    currObj = Instantiate(projectilePrefab, projectileManager.transform);
    Destroy(currObj, 4.0f);

    // Move the projectile to the RayCastHit location
    //hitSomething = Physics.Raycast(gameObject.transform.position, target, out hit, (int)(2.0 * Util.planeSize));
    hitSomething = Physics.Raycast(gameObject.transform.position, target, out hit, 2000);

    Debug.DrawRay(gameObject.transform.position, target, Color.white, 1.0f);

    if ( !hitSomething )
    {
      Debug.Log("MISSED");
      return;
    }

    // Set the location of the projectile
    currObj.transform.position = hit.point;

    // If raycast hits enemy, destroy them
    // ONLY destroy ENEMY objects
    if (hit.transform.tag == "Enemy")
    {
      hit.transform.gameObject.GetComponent<Enemy>().Kill();
    }
    // If the user shoots the buttons on the main menu
    else if ( hit.transform.tag == "ButtonTag")
    {
      Debug.Log("BUTTON CLICKED");
      Button btn = (Button) hit.transform.GetComponent("Button");
      btn.onClick.Invoke();
    }
  }

  private bool SetupControllers()
  {
    // Setup controllers, when they are added to the scene
    if (rightController == null)
    {
      rightController = GameObject.Find("RightController"); // ("Controller (right)");
    }
    if (leftController == null)
    {
      leftController = GameObject.Find("LeftController");
    }

    if (rightController != null && rightPointer == null)
    {
      //rightPointer = rightController.GetComponent("RightController/glTFController/GLTFScene/GLTFNode/GLTFNode/POINTING_POSE");
      rightPointer = rightController.transform.Find("glTFController/GLTFScene/GLTFNode/GLTFNode/POINTING_POSE").gameObject;
    }

    if (leftController != null && leftPointer == null)
    {
      //leftPointer = leftController.GetComponent("LeftController/glTFController/GLTFScene/GLTFNode/GLTFNode/POINTING_POSE");
      leftPointer = leftController.transform.Find("glTFController/GLTFScene/GLTFNode/GLTFNode/POINTING_POSE").gameObject;
    }

    if (rightController && leftController && rightPointer && leftPointer)
    {
      return true;
    }
    return false;
  }
  

 
// ########################## UTILITY FUNCTIONS  ##############################
  public float ClampAngle(float angle, float min, float max)
  {
    //if (angle & lt; -360F)
    if (angle < -360F)
             angle += 360F;
    //if (angle & gt; 360F)
    if (angle > 360F)
             angle -= 360F;
    return Mathf.Clamp(angle, min, max);
  }



  // Function to handle ALL of the camera rotation logic
  private void updateCameraView()
  {
    if (axes == RotationAxes.MouseXAndY)
    {
      // Read the mouse input axis
      rotationX += Input.GetAxis("Mouse X") * sensitivityX;
      rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
      rotationX = ClampAngle(rotationX, xRange.min, xRange.max);
      rotationY = ClampAngle(rotationY, yRange.min, yRange.max);
      Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
      Quaternion yQuaternion = Quaternion.AngleAxis(rotationY, -Vector3.right);
      transform.localRotation = originalRotation * xQuaternion * yQuaternion;
    }
    else if (axes == RotationAxes.MouseX)
    {
      rotationX += Input.GetAxis("Mouse X") * sensitivityX;
      rotationX = ClampAngle(rotationX, xRange.min, xRange.max);
      Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
      transform.localRotation = originalRotation * xQuaternion;
    }
    else
    {
      rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
      rotationY = ClampAngle(rotationY, yRange.min, yRange.max);
      Quaternion yQuaternion = Quaternion.AngleAxis(-rotationY, Vector3.right);
      transform.localRotation = originalRotation * yQuaternion;
    }
  }
}