﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    // References
    Transform moon;
    Transform sun;
    Vector3 sunStartingPos;
    GameManager gameManager;

    float timeTaken;
    float time;
    bool paused = true;
    bool finished = false;

    public void Initialize(float duration) {
        // Get reference to gameManager to notify it that the timer has ended
        gameManager = GameManager.Instance;

        // Get sun and moon and setup time
        sun = transform.GetChild(1);
        moon = transform.GetChild(0);
        timeTaken = duration;
        time = 0.0f;

        // Position sun underneath moon (shift it down by the moon's height)
        RectTransform moonDimensions = moon.GetComponent<Image>().rectTransform;
        sun.Translate(0, -moonDimensions.rect.height, 0);

        sunStartingPos = sun.position;
    }

    public void PauseTimer(bool state) {
        paused = state;
    }

    void Update() {
        MoveSunToMoon();
    }

    void MoveSunToMoon() {
        // See: https://answers.unity.com/questions/572851/way-to-move-object-over-time.html
        if (!paused && !finished) {
            time += Time.deltaTime / timeTaken;
            sun.transform.position = Vector3.Lerp(sunStartingPos, moon.transform.position, time);

            // Stop when sun reaches moon (times up)
            if (sun.transform.position == moon.transform.position) {
                finished = true;
                gameManager.EndGame(GameManager.GameResult.WIN);
            }
        }
    }
}