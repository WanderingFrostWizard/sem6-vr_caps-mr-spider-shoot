﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(IAstarAI))]
public class Ally : MonoBehaviour
{
  private IAstarAI ai;

  [Header("References")]
  public BoxCollider collisionArea;
  public Animator animator;
  public GameObject lantern;

  bool eating = false;
  bool dead = false;
  string eatingBoolTag = "Eating";
  Transform grassBeingEaten;

  // References
  GameManager gameManager; // to game manager (Enable update health + score)

  // Use this for initialization
  void Awake()
  {
        ai = GetComponent<IAstarAI>();
  }
 
  void Update()
  {
    if (eating) 
    {
       // Rotate cow to face grass
       transform.LookAt(grassBeingEaten, transform.up);
    }
    else if (!dead)
    {
       MoveToLocation(Util.allyLastPosition);
    }
  }

  void Start() {
        Util.dead = false;
        gameManager = GameManager.Instance;
        gameManager.InitScoreDisplay();
        gameManager.InitHealthDisplay();
  }

  public void MoveToLocation(Vector3 targetPoint)
  {
     ai.destination = targetPoint;
  }
  
  // ##### Eating toggling #####
  public bool IsEating() {
     return eating;
  }

  public void StartEating(Transform grass) {
      grassBeingEaten = grass;
      ToggleEating(true);
  }

  public void EndEating() {
     ToggleEating(false);
  }

  void ToggleEating(bool state) {
     eating = state;
     ai.canMove = !state;
     animator.SetBool(eatingBoolTag, state);
  }

  // Death
  public void Kill() {
    if (!dead) {
        // Stop movement
        dead = true;
        lantern.SetActive(false);
        StartCoroutine(PlayDeathAnimation());
    }
  }

  IEnumerator PlayDeathAnimation() {
        // Play death animation
        animator.Play("CowDeath");
        float animationLength = 2.0f;
        yield return new WaitForSeconds(animationLength);

        gameObject.SetActive(false);
    }
}