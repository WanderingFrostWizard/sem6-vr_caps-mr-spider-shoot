﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Pathfinding;
using UnityEngine;
using UnityEngine.Experimental.XR;
using UnityEngine.XR;

[RequireComponent(typeof(Creator))]
public class AStarLevelSetup : MonoBehaviour
{
    public float Height = 10f;
    public float Width = 1f;
    public int NumSides = 4;
    public Material LevelMaterial;

    private static readonly ReadOnlyCollection<Vector3> CapNormals = Array.AsReadOnly(new[]
    {
        Vector3.up,
        Vector3.down
    });

    private static readonly ReadOnlyCollection<Vector2Int> FaceUVs = Array.AsReadOnly(new[]
    {
        new Vector2Int(-1,  1),
        new Vector2Int( 1,  1),
        new Vector2Int( 1, -1),
        new Vector2Int(-1, -1) 
    });

    private static readonly ReadOnlyCollection<int> FaceIndices = Array.AsReadOnly(new[]
    {
        0, 1, 2, 0, 2, 3
    });

    private static readonly int VerticesPerSide = FaceUVs.Count;
    private static readonly int IndicesPerSide = FaceIndices.Count;
    private GameObject level;
    private Mesh levelMesh;
    private AstarPath astarPath;
    private NavMeshGraph navGraph;
    private MeshCollider meshCollider;
    private Creator creator;

    void Start()
    {
        level = new GameObject("LevelBox");
        CreateMesh();
        CreateNavmesh();
        creator = GetComponent<Creator>();
        UpdateBoundary();
    }

    Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

    [ContextMenu("Update Boundary")]
    public void UpdateBoundary()
    {
        if(!Application.isPlaying) return;

        if (XRDevice.SetTrackingSpaceType(TrackingSpaceType.RoomScale))
        {
            Debug.Log("Set to room scale");
        }
        else
        {
            Debug.LogWarning("Failed to set to room scale");
        }

        levelMesh.Clear();

        Vector2[] capVertices = BoundaryVertices();
        int[] capIndices = Triangulator.Triangulate(capVertices);
        int totalVertices = VerticesPerSide * capVertices.Length + capVertices.Length * 2;
        int totalIndices = IndicesPerSide * capVertices.Length + capIndices.Length * 2;
        var vertices = new Vector3[totalVertices];
        var normals = new Vector3[totalVertices];
        var triangles = new int[totalIndices];
        var uvs = new Vector2[totalVertices];

        float totalImageWidth = 4096;
        float imageWidth = 1920;
        float imageHeight = 1080;
        var uvStart = new Vector2((totalImageWidth - imageWidth) / totalImageWidth, 0.5f);
        var uvScale = new Vector2(imageWidth/totalImageWidth, 0.5f);
        // Get bounds
        bounds = new Bounds(Vector3.zero, Vector3.zero);
        foreach (Vector2 vertex in capVertices) {
            bounds.Encapsulate(new Vector3(vertex.x, 0, vertex.y) * Width);
        }
        Vector3 boundsExtents = bounds.extents;
        if (boundsExtents.x/imageWidth < boundsExtents.x/imageHeight)
        {
            boundsExtents.x = boundsExtents.z * imageWidth/imageHeight;
        }
        else
        {
            boundsExtents.z = boundsExtents.x * imageWidth / imageHeight;
        }
        bounds.extents = boundsExtents;
        var boundsMin = new Vector2(bounds.min.x, bounds.min.z);
        var boundsSize = new Vector2(bounds.size.x, bounds.size.z);
        for (var cap = 0; cap < CapNormals.Count; ++cap)
        {
            int capStart = cap * capVertices.Length;
            int capIndexStart = cap * capIndices.Length;
            for (var i = 0; i < capVertices.Length; ++i)
            {
                int idx = capStart + i;
                Vector2 vertex = capVertices[i] * Width;
                vertices[idx] = new Vector3(vertex.x, 0, vertex.y);
                if (cap == 1) vertices[idx] -= CapNormals[cap] * Height;
                normals[idx] = CapNormals[cap];
                
                // Calculate UV
                // Get vec from bounds min to vertex
                Vector2 uv = vertex - boundsMin;
                // Get distance along each axis as percentage of bounds size
                // [0, 1]
                uv.x /= boundsSize.x;
                uv.y /= boundsSize.y;

                // Convert to uv in region of texture
                uvs[idx] = Vector2.Scale(uv, uvScale) + uvStart;
            }
            for (var i = 0; i < capIndices.Length; ++i)
            {
                int index = cap == 0 ? capIndices.Length - i - 1 : i;
                triangles[capIndexStart + i] = capIndices[index] + capStart;
            }
        }

        float totalFaceLength = capVertices.Select((t, face) => Util.WrapIndex(capVertices, face + 1) - t).Sum(sideVec => sideVec.sqrMagnitude) * 2;
        float accumulatedLength = 0;
        float u = 0;
        float vTop = Height/totalFaceLength;
        for (var face = 0; face < capVertices.Length; face++)
        {
            int faceStart = CapNormals.Count * capVertices.Length + face * VerticesPerSide;
            Vector2 a = capVertices[face];
            Vector2 b = Util.WrapIndex(capVertices, face + 1);
            Vector2 sideVec = Util.WrapIndex(capVertices, face + 1) - capVertices[face];
            float nextLength = accumulatedLength + sideVec.sqrMagnitude;
            float nextU = nextLength/totalFaceLength;
            var uVec = new Vector3(sideVec.x, 0, sideVec.y);
            Vector3 normal = Vector3.Cross(uVec, Vector3.up).normalized;
            vertices[faceStart + 0] = new Vector3(a.x, 0, a.y) * Width;
            normals[faceStart + 0] = normal;
            uvs[faceStart + 0] = new Vector2(u, 0);
            vertices[faceStart + 1] = new Vector3(b.x, 0, b.y) * Width;
            normals[faceStart + 1] = normal;
            uvs[faceStart + 1] = new Vector2(nextU, 0);
            vertices[faceStart + 2] = new Vector3(b.x, 0, b.y) * Width + Vector3.up * Height;
            normals[faceStart + 2] = normal;
            uvs[faceStart + 2] = new Vector2(nextU, vTop);
            vertices[faceStart + 3] = new Vector3(a.x, 0, a.y) * Width + Vector3.up * Height;
            normals[faceStart + 3] = normal;
            uvs[faceStart + 3] = new Vector2(u, vTop);

            u = nextU;
            accumulatedLength = nextLength;

            for (var i = 0; i < IndicesPerSide; i++)
            {
                int vertexIndex = faceStart + FaceIndices[i];
                int idx = CapNormals.Count * capIndices.Length + face * IndicesPerSide + i;
                triangles[idx] = vertexIndex;
            }
        }



        levelMesh.vertices = vertices;
        levelMesh.normals = normals;
        levelMesh.triangles = triangles;
        levelMesh.uv = uvs;

        meshCollider.sharedMesh = levelMesh;

        RecalculateGraph();
        creator.Initialize(levelMesh, level.transform);
    }

    private void RecalculateGraph()
    {
        if (navGraph != null)
        {
            navGraph.sourceMesh = levelMesh;
            navGraph.recalculateNormals = false;
            astarPath.Scan();
        }
    }

    private void CreateMesh()
    {
        var meshFilter = level.AddComponent<MeshFilter>();
        var meshRender = level.AddComponent<MeshRenderer>();
        Material mat = LevelMaterial;
        if (mat == null)
        {
            GameObject primitive = GameObject.CreatePrimitive(PrimitiveType.Plane);
            primitive.SetActive(false);
            mat = primitive.GetComponent<MeshRenderer>().sharedMaterial;
            DestroyImmediate(primitive);
        }
        meshRender.sharedMaterial = mat;
        levelMesh = meshFilter.mesh;
        meshCollider = level.AddComponent<MeshCollider>();
    }

    private void CreateNavmesh()
    {
        astarPath = AstarPath.active;
        navGraph = astarPath.data.AddGraph(typeof(NavMeshGraph)) as NavMeshGraph;
        if (navGraph == null)
        {
            Debug.Log("Failed to add navmesh graph");
        }
    }

    private Vector2[] BoundaryVertices()
    {
        var boundaryVertices = new List<Vector3>();
        if ((Boundary.TryGetGeometry(boundaryVertices, Boundary.Type.TrackedArea) && boundaryVertices.Count > 0) // This is what would be used if the bug below was fixed
            || boundaryVertices.Count > 0 // Account for TryGetGeometry always returning false
           )
        {
            return boundaryVertices.Select(v => new Vector2(v.x, v.z)).ToArray();
        }
        
        // Fallback when no boundary set
        var vertices = new Vector2[NumSides];
        for (var vertex = 0; vertex < NumSides; ++vertex)
        {
            Vector3 dir = Util.VectorForSideRegularPolygon(NumSides - vertex - 1, NumSides);
            Vector3 uVec = Vector3.Cross(dir, Vector3.up) * Mathf.Tan(Mathf.PI / NumSides);
            Vector3 offset = dir + uVec;
            Vector3 pos = -offset;
            vertices[vertex] = new Vector2(pos.x, pos.z);
        }
        return vertices;
    }

    private void Update()
    {
        Debug.DrawLine(bounds.min + Vector3.down, bounds.max + Vector3.down);
    }
}
