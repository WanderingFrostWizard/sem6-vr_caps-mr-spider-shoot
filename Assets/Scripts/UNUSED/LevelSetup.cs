﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LevelSetup : MonoBehaviour
{
    public GameObject wallPrefab;
    public GameObject floorPrefab;
    public GameObject allyPrefab;
    private GameObject ally;
    private Creator creator;

    // Width of the NavMeshLinks - Recalculated from wallScaleValue
    private float linkWidth;

    // Scale factor to be added to the transforms within the wall prefabs
    private Vector3 wallScale;

    // Reference storage - to allow easy access later on
    private GameObject floor;
    private GameObject ceiling;

    // Positions of the walls
    private Vector3[] wallPos;

    // Rotations to position each of the walls in the same order as above
    private Vector3 [] wallRot = new [] { new Vector3( 0, 0,  90),
                                          new Vector3( 0, 0, -90),
                                          new Vector3( -90, 0, 0 ),
                                          new Vector3(  90, 0, 0 )  };

    // List of each of the NavMeshLink start and end locations    
    private Vector3[] linkList;

    // Current GameObject
    private GameObject curr;


    // Use this for initialization
    void Start ()
    {
        creator = this.GetComponent<Creator>();
        calculateWallSizes();

        // Find the LevelLayout Object
        Util.levelLayout = GameObject.Find("LevelLayout");
        if (Util.levelLayout == null )
          Debug.LogError("Cannot find LevelLayout!");

        // Temp variable to allow for changes in the sizes of the arrays
        int len = wallPos.Length;

        // Create walls
        for (int i = 0; i < len; ++i )
        {
            createWall( wallPos[i], wallRot[i] );
        }

        createFloor();
        createCeiling();
        createNavLinks();

        //// Move the LevelLayout up to have world at 0,0,0
        //Vector3 pos = gameObject.transform.position;
        //pos.y += (0.5f * Util.planeSize);
        //gameObject.transform.position = pos;

        ally = Instantiate(allyPrefab);
        ally.transform.position = new Vector3(Util.planeSize, 0, 0);
        Util.ally = ally;

        creator.Initialize();
    }

    private void calculateWallSizes()
    {
      // Calculate the value of HALF of the current wall size
      Util.planeSize = Util.PLANEORIGSIZE * Util.wallScaleValue / 2;

      wallScale.x = Util.wallScaleValue;
      wallScale.y = Util.wallScaleValue;
      wallScale.z = Util.wallScaleValue;

      // Check the size of each wall panel
      //Plane temp = (Plane) wallPrefab.GetComponent("Plane");
      wallPrefab.transform.GetChild(0).localScale = wallScale;
      floorPrefab.transform.GetChild(0).localScale = wallScale;

      // Wall Positions based upon calculations above in this order  Left, Right, Front, Rear
      wallPos = new[] { new Vector3(  Util.planeSize, 0, 0),
                        new Vector3( -Util.planeSize, 0, 0),
                        new Vector3( 0, 0,  Util.planeSize ),
                        new Vector3( 0, 0, -Util.planeSize )  };

      // UPDATE the List of each of the NavMeshLink start and end locations, based upon the calculations above  
      linkList = new[] {  // Right Wall bottom start, end
            new Vector3( Util.planeSize - Util.AGENTSIZE - Util.PADDING,    -Util.planeSize,   0 ),
            new Vector3( Util.planeSize,     -Util.planeSize + Util.AGENTSIZE + Util.PADDING,  0 ),
            // Right Wall top start, end
            new Vector3( Util.planeSize - Util.AGENTSIZE - Util.PADDING,    Util.planeSize,    0 ),
            new Vector3( Util.planeSize,     Util.planeSize - Util.AGENTSIZE - Util.PADDING,   0 ),

            // Left Wall bottom, top
            new Vector3( -Util.planeSize + Util.AGENTSIZE + Util.PADDING,    -Util.planeSize,   0 ),
            new Vector3( -Util.planeSize,     -Util.planeSize + Util.AGENTSIZE + Util.PADDING,  0 ),
            // Left Wall top start, end
            new Vector3( -Util.planeSize + Util.AGENTSIZE + Util.PADDING,    Util.planeSize,    0 ),
            new Vector3( -Util.planeSize,     Util.planeSize - Util.AGENTSIZE - Util.PADDING,   0 ),

            // Front Wall bottom, top
            new Vector3( 0,    -Util.planeSize,   Util.planeSize - Util.AGENTSIZE - Util.PADDING ),
            new Vector3( 0,    -Util.planeSize + Util.AGENTSIZE + Util.PADDING,  Util.planeSize ),
            // Front Wall top start, end
            new Vector3( 0,    Util.planeSize,    Util.planeSize - Util.AGENTSIZE - Util.PADDING ),
            new Vector3( 0,    Util.planeSize - Util.AGENTSIZE - Util.PADDING,   Util.planeSize ),
                                        
            // Back Wall bottom, top
            new Vector3( 0,    -Util.planeSize,   -Util.planeSize + Util.AGENTSIZE + Util.PADDING ),
            new Vector3( 0,    -Util.planeSize + Util.AGENTSIZE + Util.PADDING,  -Util.planeSize ),
            // Back Wall top start, end
            new Vector3( 0,    Util.planeSize,    -Util.planeSize + Util.AGENTSIZE + Util.PADDING ),
            new Vector3( 0,    Util.planeSize - Util.AGENTSIZE - Util.PADDING,   -Util.planeSize ),

            // TODO VERTICAL NAVMESH LINKS ######################################
            // I don't believe the current version of NavMesh allows for vertical NavMeshLinks

            // Right Wall to Front Wall start, end
            
            // Right Wall to Rear Wall  start, end
            //new Vector3( planeSize,   0,  planeSize - AGENTSIZE - PADDING ),
            //new Vector3( planeSize - AGENTSIZE - PADDING, 0,    planeSize )

            // Left Wall to Front Wall  start, end

            // Left Wall to Read Wall   start, end
      };

      // Calculations are   (Width of Plane) - (2 * Size of AI Agent) - (2 * Util.PADDING Size)
      linkWidth = (Util.PLANEORIGSIZE * Util.wallScaleValue) - (2 * Util.AGENTSIZE) - (2 * Util.PADDING);
    }

    // Instantiate and position a SINGLE wall object
    private void createWall(Vector3 pos, Vector3 rot)
    {
      // Add the next wall to the parent object
      curr = Instantiate(wallPrefab, Util.levelLayout.transform);

      // Set the wall to the correct location
      curr.transform.position = pos;

      // Rotate the wall into position
      curr.transform.Rotate(rot);

      initNavPoints(curr);
    }

    // Instantiate and position the floor object
    private void createFloor( )
    {
      // Add the next wall to the parent object
      floor = Instantiate(floorPrefab, Util.levelLayout.transform);

      // Set the wall to the correct location
      floor.transform.position = new Vector3( 0, -Util.planeSize, 0 );

      initNavPoints(floor);
    }

    // Instantiate and position the ceiling object
    private void createCeiling()
    {  
      // Add the next wall to the parent object
      ceiling = Instantiate(floorPrefab, Util.levelLayout.transform);

      // Set the ceiling to the correct location
      ceiling.transform.position = new Vector3(0, Util.planeSize, 0);

      // Flip the floor prefab to become a ceiling
      ceiling.transform.Rotate( new Vector3( 180, 0, 0 ) );

      initNavPoints(ceiling);
    }

    /*
     * Functions to bake NAV points to the containerObject, before building them
     */
    private void initNavPoints( GameObject containerObject )
    {
      NavMeshSurface surfaceTemp = containerObject.AddComponent(typeof(NavMeshSurface)) as NavMeshSurface;
      NavMeshBuildSettings buildSetTemp = surfaceTemp.GetBuildSettings();
      buildSetTemp.agentHeight = Util.AGENTSIZE;
      buildSetTemp.agentRadius = Util.AGENTSIZE;
      surfaceTemp.collectObjects = CollectObjects.Children;
      surfaceTemp.BuildNavMesh();
    }

    /*
     * Function to add NavMeshLinks to the top and bottom of each wall
     */
    private void createNavLinks()
    {
      NavMeshLink link;

      for (int i = 0; i < linkList.Length; i += 2)
      {
        // Create a NavMeshLink in the LevelLayout Object
        link = gameObject.AddComponent(typeof(NavMeshLink)) as NavMeshLink;

        // Set the start and End points
        link.startPoint = linkList[i];
        link.endPoint = linkList[i + 1];
        link.width = linkWidth;
      }
    }
}
