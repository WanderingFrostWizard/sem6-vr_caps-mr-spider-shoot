﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Conversation : MonoBehaviour {

    // References
    GameObject playerTextObj;
    GameObject cowTextObj;
    GameObject quitObj;
    GameObject playObj;
    GameObject skipObj;
    Text cowText;
    Text playerText;
    Button playerTextButton;
    Button skipButton;

    // Dialogue related variables
    int dialogueIndex = 0;
    bool canProceed = true;
    float playerDialogueDelayTime = 1.0f;

    // NOTE: Number of lines should be equal for both the cow and player
    string[] cowDialogue = {
        "Hey!",
        "Look buddy, sorry to break it to you but...",
        "You died (~o~), that's what",
        "But you can get a second chance",
        "IF, you guide me. I need sustenance...",
        "and that sustenance will bring you to redemption",
        "(>-<) don't give me that look",
        "Look just tell me when you're ready",
        "Hey! Listen!"
    };

    string[] playerDialogue = {
        "...",
        "What happened?",
        "Oh...",
        "CONTINUE",
        "CONTINUE",
        "CONTINUE",
        ".....",
        "",
    };

    //Even lines (starting at 0) are the cow's, odd lines are the player's
    string[] dialogueBoth =
    {
        "Wake up!",
        "...",
        "Hey! Listen!",
        ">.<",
        "Look, we don't have a lot of time...",
        "???",
        "You're dead.",
        "!!!",
        "Or *we're* dead, I should say.",
        "!?!?!?",
        "Who am I? I'm you. Or part of you.",
        "??????",
        "The part that deals with survival. Eating. Not being eaten.",
        "o.O",
        "Look, this is the underworld. It's... not a good place to be at night.",
        "<.<",
        "If we can survive 'til sunrise, there's still hope. To return to life or to move on.",
        ">.>",
        "But there are... things down here. That eat lost souls like us.",
        "D:",
        "And I'm getting weaker. Eating the spirit grass seems to help, though.",
        "...",
        "You'll need to protect me, and tell me where to go. You know, like usual.",
        "..."
    };

    //string[] dialogueBoth =
    //{"Wake up!",
    //    "...",
    //    "Hey! Listen!",
    //    ">.<",
    //    "Look, we don't have a lot of time.",
    //    "???",
    //    "Well, um. You're... dead.",
    //    "!!!!!!",
    //    "Or, I should say, *we're* dead.",
    //    "!?!?!?",
    //    "Who am I? I'm you. Or part of you.",
    //    "o.O",
    //    "I'm the part that keeps you alive. Food, survival, that sort of thing.",
    //    "...",

    //    /*"Wake up!",
    //    "...",
    //    "Hey! Listen!",
    //    ">.<",
    //    "Look, we don't have a lot of time.",
    //    "???",
    //    "Well, um. You're... dead.",
    //    "!!!!!!",
    //    "Yeah, I know.",
    //    "!?!?!?",
    //    "Who am I? I'm, well, you.",
    //    "o.O",
    //    "I'm the part that deals with basic stuff. Food, survival, you know.",
    //    "...",
    //    "Look, I don't know how we died.",
    //    ">.<",
    //    "Knowing that stuff is *your* job.",
    //    "-.-",
    //    "What *I* know is that this isn't over. We can come back. Or I wouldn't still be here.",
    //    "!!!???",*/

    //    /*"Look, I don't know how.",
    //    "...",
    //    "Knowing things mostly isn't my job, OK? It's yours.",
    //    ">.<",
    //    "Who am I? I'm you.",
    //    "...",
    //    "I'm the part of you that takes care of survival. Mostly eating.",
    //    "-.-",
    //    "*No*, we did not starve to death.",
    //    "...",
    //    "I *said* I don't know! But it wasn't my fault!",
    //    "...",*/

    //};

    void Start() {
        Initialize();
    }

    void Initialize() {
        // Setup references
        cowTextObj = transform.Find("CowTextBox/CowText").gameObject;
        playerTextObj = transform.Find("PlayerTextBox/PlayerText").gameObject;
        quitObj = transform.Find("QuitButton").gameObject;
        playObj = transform.Find("PlayButton").gameObject;
        skipObj = transform.Find("SkipButton").gameObject;

        cowText = cowTextObj.GetComponent<Text>();
        playerText = playerTextObj.GetComponent<Text>();

        skipButton = transform.Find("SkipButton").GetComponent<Button>();
        playerTextButton = transform.Find("PlayerTextBox").GetComponent<Button>();

        // Setup visibility
        quitObj.SetActive(false);
        playObj.SetActive(false);
        playerTextButton.interactable = false;

        // Start the dialogue
        UpdateDialogue();
    }

    IEnumerator PrintDialouge() {
        // Prevent continuation, show cow text
        canProceed = false;
        playerTextButton.interactable = false;
        skipButton.interactable = false;
        playerText.text = "";
        //cowText.text = cowDialogue[dialogueIndex];
        cowText.text = dialogueBoth[dialogueIndex++];

        yield return new WaitForSeconds(playerDialogueDelayTime);

        // Update player text, move to next set of dialogue, re-enable continuation
        //playerText.text = playerDialogue[dialogueIndex];
        playerText.text = dialogueBoth[dialogueIndex++];
        //dialogueIndex++;
        canProceed = true;
        playerTextButton.interactable = true;
        skipButton.interactable = true;

        // Do once, set quit option active
        if (dialogueIndex == dialogueBoth.Length)
            FinishConversation();
    }

    void FinishConversation() {
        canProceed = false;

        // Toggle visibility of objects
        playObj.SetActive(true);
        quitObj.SetActive(true);
        skipObj.SetActive(false);
        playerTextObj.SetActive(false);

        // Prevent player dialogue from continuing
        playerTextButton.interactable = false;
    }

    #region Button callable functions
    public void UpdateDialogue() {
        if (canProceed)
            StartCoroutine(PrintDialouge());
    }

    public void SkipConversation() {
        // Toggle visibility of objects
        playerTextObj.SetActive(false);
        skipObj.SetActive(false);
        playObj.SetActive(true);
        quitObj.SetActive(true);

        // Talking is done
        canProceed = false;

        // Jump to final dialogue choice
        dialogueIndex = dialogueBoth.Length - 1;
        cowText.text = dialogueBoth[dialogueIndex];
    }
    #endregion
}
