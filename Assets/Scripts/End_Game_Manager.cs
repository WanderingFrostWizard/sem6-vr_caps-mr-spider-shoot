﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class End_Game_Manager : MonoBehaviour
{
  public Text spiderKillsText;
  public Text grassEatenText;
  public GameObject WinWindow;
  public GameObject LoseWindow;    

	// Use this for initialization
  void Start ()
  {
    if ( EndGameData.result == GameManager.GameResult.WIN )
    {
      WinWindow.SetActive(true);
    }
    else
    {
      LoseWindow.SetActive(true);
    }

    spiderKillsText.text = EndGameData.spiderKills.ToString();
    grassEatenText.text = EndGameData.grassEaten.ToString();
  }

    public static class EndGameData
    {
        public static int spiderKills;
        public static int grassEaten;
        public static GameManager.GameResult result;
    }
}
